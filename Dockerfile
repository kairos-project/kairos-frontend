FROM node:lts-alpine as build

RUN apk add yarn

FROM build as dist

WORKDIR /app
COPY package*.json ./

RUN yarn

COPY . .
RUN yarn --verbose run build

FROM nginx:latest as result

COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=dist /app/build /usr/share/nginx/html
