import { createStore, Store, Event, createEvent, Effect, createEffect } from "effector";
import { Lecturer } from "api/models/lecturers/Lecturer";
import { LecturerType } from "api/models/lecturers/LecturerType";
import { getAcademicRanks, getDegrees, getLecturerTypes } from "api/directory";
import { getLecturers } from "api/lecturers";
import { AcademicRank } from "api/models/lecturers/AcademicRank";
import { Degree } from "api/models/lecturers/Degree";

interface LecturersData{
    lecturers: Lecturer[]
    lecturerTypes: LecturerType[]
    degrees: Degree[]
    academicRanks: AcademicRank[]
}

const initialData: LecturersData = {
    lecturers: [],
    lecturerTypes: [],
    academicRanks: [],
    degrees: []
};

interface LecturersStore extends Store<LecturersData> {
    setLecturerTypes: Event<LecturerType[]>
    setDegrees: Event<Degree[]>
    setAcademicRanks: Event<AcademicRank[]>
    setLecturers: Event<Lecturer[]>
    getLecturers: Effect<void, Lecturer[]>
    getLecturerTypes: Effect<void, LecturerType[]>
    getAcademicRanks: Effect<void, AcademicRank[]>
    getDegrees: Effect<void, Degree[]>
}


export const LecturersStore = (() => {
    const store = createStore<LecturersData>(initialData) as LecturersStore;

    store.setLecturers = createEvent<Lecturer[]>("set lecturers");
    store.on(store.setLecturers, (((state, payload) => ({ ...state, lecturers: payload }))));

    store.setLecturerTypes = createEvent<LecturerType[]>("set lecturer types");
    store.on(store.setLecturerTypes, ((state, payload) => ({ ...state, lecturerTypes: payload })));

    store.setDegrees = createEvent<Degree[]>("set degrees");
    store.on(store.setDegrees, ((state, payload) => ({ ...state, degrees: payload })));

    store.setAcademicRanks = createEvent<AcademicRank[]>("set academic ranks");
    store.on(store.setAcademicRanks, ((state, payload) => ({ ...state, academicRanks: payload })));

    store.getLecturers = createEffect({
        name: "get lecturers list",
        handler: async (): Promise<Lecturer[]> => {
            const response = await getLecturers();
            return response;
        }
    });
    store.getLecturers.done.watch(result => store.setLecturers(result.result));

    store.getLecturerTypes = createEffect({
        name: "get lecturer types list",
        handler: async (): Promise<LecturerType[]> => {
            const response = await getLecturerTypes();
            return response;
        }
    });
    store.getLecturerTypes.done.watch(result => store.setLecturerTypes(result.result));

    store.getDegrees = createEffect({
        name: "get degrees",
        handler: async (): Promise<Degree[]> => {
            const response = await getDegrees();
            return response;
        }
    });
    store.getDegrees.done.watch(result => store.setDegrees(result.result));

    store.getAcademicRanks = createEffect({
        name: "get academic ranks",
        handler: async (): Promise<AcademicRank[]> => {
            const response = await getAcademicRanks();
            return response;
        }
    });
    store.getAcademicRanks.done.watch(result => store.setAcademicRanks(result.result));


    store.getLecturers();
    store.getLecturerTypes();
    store.getAcademicRanks();
    store.getDegrees();
    return store;
})();


