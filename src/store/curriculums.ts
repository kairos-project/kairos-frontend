import { createStore, Store, Event, createEvent, Effect, createEffect } from "effector";
import { Curriculum } from "api/models/curriculums/Curriculum";
import { getCurriculums } from "api/curriculums";

interface CurriculumsData {
    curriculums: Curriculum[]
}

const initialData: CurriculumsData = {
    curriculums: []
};

interface CurriculumsStore extends Store<CurriculumsData> {
    setCurriculums: Event<Curriculum[]>
    getCurriculums: Effect<void, Curriculum[] >
}


export const CurriculumsStore = (() => {
    const store = createStore<CurriculumsData>(initialData) as CurriculumsStore;

    store.setCurriculums = createEvent<Curriculum[]>("set curriculums");
    store.on(store.setCurriculums, (((state, payload) => ({ ...state, curriculums: payload }))));

    store.getCurriculums = createEffect({
        name: "get curriculums list",
        handler: async (): Promise<Curriculum[]> => {
            const response = await getCurriculums();
            return response;
        }
    });
    store.getCurriculums.done.watch(result => store.setCurriculums(result.result));

    store.getCurriculums();
    
    return store;
})();


