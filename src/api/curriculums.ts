import { backendUrl, http } from "api/http";
import { Curriculum, DtoCurriculum } from "api/models/curriculums/Curriculum";
import { trackPromise } from "react-promise-tracker";
import { CurriculumFull, DtoCurriculumFull } from "api/models/curriculums/CurriculumFull";

export const getCurriculums = async (): Promise<Curriculum[]> => {
    const args = {};
    const response = await http.get<DtoCurriculum[]>("/curriculums", args);
    return response.map(dto => Curriculum.fromDto(dto));
};

export const deleteCurriculum = async (id: string): Promise<void> => {
    const args = {};
    await http.delete<void>(`/curriculums/${id}`, args);
    return;
};

export const getFullCurriculum  = async (id: string): Promise<CurriculumFull> => {
    const args = {};
    const response = await http.get<DtoCurriculumFull>(`/curriculums/${id}`, args);
    return CurriculumFull.fromDto(response);
};

// not sorry
export const uploadCurriculumFile = async (file: File): Promise<void> => {
    const formData = new FormData();
    formData.append("file", file, file.name);
    const requestInit: RequestInit = {
        method: "POST",
        body: formData
    };
    await trackPromise(fetch(`${backendUrl}/curriculums`, requestInit));
};