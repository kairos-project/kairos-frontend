import { http } from "./http";
import { DtoLecturer, Lecturer, LecturerTemplate } from "api/models/lecturers/Lecturer";

export const getLecturers = async (): Promise<Lecturer[]> => {
    const args = {};
    const response = await http.get<DtoLecturer[]>("/lecturers", args);
    return response.map(dto => Lecturer.fromDto(dto));
};

export const deleteLecturer = async (id: string): Promise<void> => {
    const args = {};
    await http.delete<void>(`/lecturers/${id}`, args);
    return;
};

export const addLecturer = async (lecturer: LecturerTemplate): Promise<void> => {
    const args = {};
    await http.post("/lecturers", args, JSON.stringify(LecturerTemplate.toDto(lecturer)));
    return;
};