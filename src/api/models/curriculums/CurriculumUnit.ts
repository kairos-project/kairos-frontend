import { AutoDto, AutoModel } from "utils/model";

interface CurriculumUnitMD {
    course: number
    curriculumId: string
    id: string
    load: number
    loadType: string
    semester: number
    subject: string
}

export type CurriculumUnit = AutoModel<CurriculumUnitMD>;
export type DtoCurriculumUnit = AutoDto<CurriculumUnitMD>;

export const CurriculumUnit = {
    fromDto: (obj: DtoCurriculumUnit): CurriculumUnit => {
        return {
            ...obj
        };
    }
};
