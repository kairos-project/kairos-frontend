import { AutoDto, AutoModel, MDMap } from "utils/model";
import { DtoSubject, Subject } from "api/models/curriculums/Subject";

interface CurriculumFullMD {
    subjects: MDMap<Subject, DtoSubject>[]
}

export type CurriculumFull = AutoModel<CurriculumFullMD>;
export type DtoCurriculumFull = AutoDto<CurriculumFullMD>;

export const CurriculumFull = {
    fromDto: (obj: DtoCurriculumFull): CurriculumFull => {
        return {
            ...obj
        };
    }
};