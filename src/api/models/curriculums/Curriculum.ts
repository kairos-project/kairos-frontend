import { uuid } from "utils/types";
import { AutoDto, AutoModel } from "utils/model";

interface CurriculumMD {
    id: uuid
    fieldOfStudy: string
    profile: string
    startYear: number
}

export type Curriculum = AutoModel<CurriculumMD>;
export type DtoCurriculum = AutoDto<CurriculumMD>;

export const Curriculum = {
    fromDto: (obj: DtoCurriculum): Curriculum => {
        return {
            ...obj
        };
    }
};