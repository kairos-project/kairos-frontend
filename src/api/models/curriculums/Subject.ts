import { AutoDto, AutoModel, MDMap } from "utils/model";
import { CurriculumUnit, DtoCurriculumUnit } from "api/models/curriculums/CurriculumUnit";

interface SubjectMD {
    course: number
    curriculumUnit: MDMap<CurriculumUnit, DtoCurriculumUnit>[]
    semester: number
    subject: string
}

export type Subject = AutoModel<SubjectMD>;
export type DtoSubject = AutoDto<SubjectMD>;

export const Subject = {
    fromDto: (obj: DtoSubject): Subject => {
        return {
            ...obj
        };
    }
};