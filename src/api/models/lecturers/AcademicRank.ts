import { AutoModel } from "utils/model";
import { AutoDto } from "utils/model/types";

interface AcademicRankMD {
    academicRank: string
}

export type AcademicRank = AutoModel<AcademicRankMD>;
export type DtoAcademicRank = AutoDto<AcademicRankMD>;

export const AcademicRank = {
    fromDto: (a: DtoAcademicRank): AcademicRank => {
        return {
            ...a
        };
    }
};