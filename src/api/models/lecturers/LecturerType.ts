import { AutoDto, AutoModel } from "utils/model";
import { uuid } from "utils/types";

interface LecturerTypeMD {
    id: uuid
    type: string
    studyLoad: number
    isPartTime: boolean
    isExternal?: boolean
}

export type LecturerType = AutoModel<LecturerTypeMD>;
export type DtoLecturerType = AutoDto<LecturerTypeMD>;

export const LecturerType = {
    fromDto: (obj: DtoLecturerType) => {
        return {
            ...obj,
            isExternal: obj.isExternal ?? undefined
        };
    }
};