import { asDate, asDecimal, AutoDto, AutoModel, MDMap } from "utils/model";
import { uuid } from "utils/types";
import Decimal from "decimal.js";
import { DtoLecturerType, LecturerType } from "./LecturerType";
import { AcademicRank, DtoAcademicRank } from "api/models/lecturers/AcademicRank";
import { Degree, DtoDegree } from "api/models/lecturers/Degree";

interface LecturerMD {
    id: uuid
    lecturerType: MDMap<LecturerType, DtoLecturerType>
    academicRank: MDMap<AcademicRank, DtoAcademicRank>
    degree: MDMap<Degree, DtoDegree>
    name: string
    middleName: string
    lastName: string
    lecturerRate: Decimal
    lecturerLoadForRate?: number
    lecturerMaxLoadForRate?: number
    monthAmount: Decimal
    employmentStartDate: Date
    employmentFinishDate: Date
}

export type Lecturer = AutoModel<LecturerMD>;
export type DtoLecturer = AutoDto<LecturerMD>;

export const Lecturer = {
    fromDto: (obj: DtoLecturer) => {
        return {
            ...obj,
            lecturerRate: asDecimal(obj.lecturerRate),
            lecturerLoadForRate: obj.lecturerLoadForRate ?? undefined,
            lecturerMaxLoadForRate: obj.lecturerMaxLoadForRate ?? undefined,
            employmentStartDate: asDate(obj.employmentStartDate),
            employmentFinishDate: asDate(obj.employmentFinishDate),
            monthAmount: asDecimal(obj.monthAmount)
        };
    }
};

interface LecturerTemplateMD {
    id?: string
    lecturerTypeId: string
    name: string
    degree: Degree
    academicRank: AcademicRank
    middleName: string
    lastName: string
    lecturerRate: Decimal
    employmentStartDate: Date
    employmentFinishDate: Date
    monthAmount: Decimal
}

export type LecturerTemplate = AutoModel<LecturerTemplateMD>;
export type DtoLecturerTemplate = AutoDto<LecturerTemplateMD>;

export const LecturerTemplate = {
    toDto: (obj: LecturerTemplate): DtoLecturerTemplate => {
        return {
            ...obj,
            employmentStartDate: obj.employmentStartDate.toISOString(),
            employmentFinishDate: obj.employmentFinishDate.toISOString(),
            lecturerRate: obj.lecturerRate.toString(),
            monthAmount: obj.monthAmount.toString()
        };
    }
};