import { AutoModel } from "utils/model";
import { AutoDto } from "utils/model/types";

interface DegreeMD {
    degree: string
}

export type Degree = AutoModel<DegreeMD>;
export type DtoDegree = AutoDto<DegreeMD>;

export const Degree = {
    fromDto: (a: DtoDegree): Degree => {
        return {
            ...a
        };
    }
};