import { DtoLecturerType, LecturerType } from "api/models/lecturers/LecturerType";
import { http } from "./http";
import { AcademicRank, DtoAcademicRank } from "api/models/lecturers/AcademicRank";
import { Degree, DtoDegree } from "api/models/lecturers/Degree";

export const getLecturerTypes = async (): Promise<LecturerType[]> => {
    const args = {};
    const response = await http.get<DtoLecturerType[]>("/directory/lecturerTypes", args);
    return response.map(dto => LecturerType.fromDto(dto));
};

export const getAcademicRanks = async (): Promise<AcademicRank[]> => {
    const args = {};
    const response = await http.get<DtoAcademicRank[]>("/directory/academicRanks", args);
    return response.map(dto => AcademicRank.fromDto(dto));
};

export const getDegrees = async (): Promise<Degree[]> => {
    const args = {};
    const response = await http.get<DtoDegree[]>("/directory/degrees", args);
    return response.map(dto => Degree.fromDto(dto));
};

