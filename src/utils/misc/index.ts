export * from "./predicates";
export * from "./debouncing";
export * from "./compare";
export * from "./objects";

export const noop = (): void => {return;};