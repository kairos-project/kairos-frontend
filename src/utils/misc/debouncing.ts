import { AsyncFn, Fn } from "utils/types";

export function debounced<A extends unknown[]>(delay: number, fn: Fn<A, void>): Fn<A, void> {
    let timerId: NodeJS.Timeout | undefined;

    return (...args) => {
        if (timerId) {
            clearTimeout(timerId);
        }
        timerId = setTimeout(() => {
            fn(...args);
            timerId = undefined;
        }, delay);
    };
}

export type DebouncingResult<T> = { canceled: false; result: T } | { canceled: true };

export function debouncedAsync<A extends unknown[], R>(
    delay: number,
    fn: AsyncFn<A, R>
): AsyncFn<A, DebouncingResult<R>> {
    let timerId: NodeJS.Timeout | undefined;
    let promiseResolve: ((value?: DebouncingResult<R>) => void) | undefined;

    return (...args) => {
        if (timerId) {
            clearTimeout(timerId);
            promiseResolve?.({ canceled: true });
        }

        const promise = new Promise<DebouncingResult<R>>(rs => { promiseResolve = rs; });
        timerId = setTimeout(async () => {
            const result = await fn(...args);
            promiseResolve?.({ canceled: false, result });
            promiseResolve = undefined;
            timerId = undefined;
        }, delay);

        return promise;
    };
}