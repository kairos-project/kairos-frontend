export const known = <T>(x: T | null | undefined): x is T => x !== null && x !== undefined;

export type ClassMap = {[key: string]: boolean | undefined | null};

export const c = (...classes: (ClassMap | string | undefined | null)[]): string =>
    classes
        .filter(known)
        .flatMap(x => typeof(x) === "string"
            ? [x] : Object.entries(x).filter(([, r]) => r).map(([c]) => c))
        .join(" ");