export declare type UnionStrings<T extends string> = {
    readonly [key in T]: string
};

export declare interface SelectOption<T> {
    key: T | undefined
    desc: string
    disabled?: boolean
}

export function toSelectOptions<T extends string>(strings: UnionStrings<T>, placeholder?: string): SelectOption<T>[] {
    const opts = Object.entries(strings).map(x => ({ key: x[0] as T, desc: x[1] } as SelectOption<T>));

    if (placeholder) {
        opts.splice(0, 0, { key: undefined, desc: placeholder, disabled: true });
    }

    return opts;
}