import { None } from "utils/types";
import Decimal from "decimal.js";

export const asDecimal = <S extends string | None>(s: S): Decimal | (S extends None ? undefined : never) =>
    s !== null && s !== undefined ? new Decimal(s as string) : undefined as never;

export const asDate = <S extends string | None>(s: S): Date | (S extends None ? undefined : never) =>
    s !== null && s !== undefined ? new Date(s as string) : undefined as never;