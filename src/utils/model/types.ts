import Decimal from "decimal.js";
import { FixNone } from "utils/types";

declare const dtoTSymbol: unique symbol;
declare const tSymbol: unique symbol;

export type MDMap<T, DtoT> = { readonly [dtoTSymbol]: DtoT; readonly [tSymbol]: T };

export interface MDSerializer<T, DtoT> {
    toDto(t: T): DtoT
}
export interface MDDeserializer<T, DtoT> {
    fromDto(dtoT: DtoT): T
}

export type MDMapper<T, DtoT> = MDSerializer<T, DtoT> & MDDeserializer<T, DtoT>;

type MDAsDto<T> =
    T extends MDMap<infer NonDtoT, infer DtoT>
        ? DtoT | FixNone<Exclude<T, MDMap<NonDtoT, DtoT>>>
        : T extends (infer A)[]
            ? MDAsDto<A>[] | FixNone<Exclude<T, A[]>>
            : T extends Date
                ? string | FixNone<Exclude<T, Date>>
                : T extends Decimal
                    ? string | FixNone<Exclude<T, Decimal>>
                    : FixNone<T>;

type MDAsModel<T> =
    T extends MDMap<infer NonDtoT, infer DtoT>
        ? NonDtoT | FixNone<Exclude<T, MDMap<NonDtoT, DtoT>>>
        : T extends (infer A)[]
            ? MDAsModel<A>[] | FixNone<Exclude<T, A[]>>
            : FixNone<T>;

export type AutoDto<T> = {
    readonly [key in keyof T]: MDAsDto<T[key]>;
};

export type AutoModel<T> = {
    readonly [key in keyof T]: MDAsModel<T[key]>;
};