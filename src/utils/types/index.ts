// simple types
export type uuid = string;
export type Defined = number | string | object | boolean | symbol;

// math
export type Minus<T, U> = Pick<T, Exclude<keyof T, keyof U>>;
export type UnionToIntersection<U> =
// eslint-disable-next-line @typescript-eslint/no-explicit-any
    (U extends any ? (k: U) => void : never) extends ((k: infer I) => void) ? I : never;

// function types
export type Fn<A extends unknown[], R> = (...args: A) => R;
export type AsyncFn<A extends unknown[], R> = (...args: A) => Promise<R>;

// nullability
export type None = null | undefined;
export type FixNone<T> = T extends undefined ? T | None : T extends null ? T | None : T;

// type assertion
export type TypeError<What extends string> = What | never;
export const staticAssert = <T>(v: T): T => v;