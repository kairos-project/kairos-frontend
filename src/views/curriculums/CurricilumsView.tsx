import { Page } from "components/layout/Page";
import styles from "views/lecturers/LecturersView.module.css";
import { Expandee, HStack } from "components/layout/flex";
import { Label } from "components/simple/Label";
import { Button } from "components/simple/Button";
import { showModal } from "components/modals";
import React, { useState } from "react";
import { useMappedStore } from "utils/store";
import { CurriculumsStore } from "store/curriculums";
import { Curriculum } from "api/models/curriculums/Curriculum";
import { Column, Table } from "components/table/Table";
import { AddCurriculumFileModal } from "views/curriculums/AddCurriculumFileModal";
import { deleteCurriculum, getFullCurriculum, uploadCurriculumFile } from "api/curriculums";
import { ConfirmationModal } from "views/modals/ConfirmationModal";
import { ContextMenuHolder } from "components/simple/ContextMenuHolder";
import { ViewCurriculumModal } from "views/curriculums/ViewCurriculumModal";

const DropdownCell: React.FC<{item: Curriculum}> = x => {
    const [contextShown, setContextShown] = useState(false);
    const contextItems: ([ string, () => void ] | "sep")[] = [
        ["Просмотр", async () => {
            const curriculumFull = await getFullCurriculum(x.item.id);
            if (curriculumFull) {
                await showModal(ViewCurriculumModal, { full: curriculumFull });
            }
        }],
        "sep",
        ["Удалить", async () => {
            const confirm = await showModal(ConfirmationModal,
                { text: "Вы действительно хотите удалить учебный план?" });
            if (confirm) {
                await deleteCurriculum(x.item.id);
                await CurriculumsStore.getCurriculums();
            }

        }]
    ];
    return <ContextMenuHolder shown={ contextShown }
                              hide={ () => setContextShown(false) }
                              items={ contextItems }>
        <Button text="Действие"
                onClick={ () => setContextShown(true) }
                presets={ ["regular", "inline"] } />
    </ContextMenuHolder>;
};

export const CurriculumsView: React.FC = () => {
    const curriculums = useMappedStore(CurriculumsStore, x => x.curriculums);

    const columns: Column<Curriculum>[] = [
        Table.Column("Направление", a => <div>{ a.item.fieldOfStudy }</div>),
        Table.Column("Профиль", a => <div>{ a.item.profile }</div>),
        Table.Column("Год начала", a => <div>{ a.item.startYear }</div>),
        Table.AutoColumn("Действие", DropdownCell)
    ];
    return <Page paper className={ styles.lecturersView }>
        <HStack alignItems="center">
            <Label text="Учебные планы" presets={ ["h3", "black"] } />
            <Expandee />
            <Button text="Добавить"
                    presets={ ["regular"] }
                    onClick={ async () => {
                        const file = await showModal(AddCurriculumFileModal, {});
                        if (file) {
                            await uploadCurriculumFile(file);
                            await CurriculumsStore.getCurriculums();
                        }
                    } } />
        </HStack>
        <Table<Curriculum> dataset={ curriculums } columns={ columns } />
    </Page>;
};
