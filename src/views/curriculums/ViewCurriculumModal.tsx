import React, { useState } from "react";
import { CardModal, ModalFC, showModal } from "components/modals";
import { Expandee, HSpread, HStack, VStack } from "components/layout/flex";
import { Button } from "components/simple/Button";
import { ConfirmationModal } from "views/modals/ConfirmationModal";
import { CurriculumFull } from "api/models/curriculums/CurriculumFull";
import { Column, Table } from "components/table/Table";
import { Subject } from "api/models/curriculums/Subject";
import { TextBox } from "components/simple/TextBox";
import { ComboBox } from "components/simple/ComboBox";
import { SelectOption } from "utils/model";
import { ContextMenuHolder } from "components/simple/ContextMenuHolder";
import { EditSubjectModal } from "views/curriculums/EditSubjectModal";

const DropdownCell: React.FC<{item: Subject}> = x => {
    const [contextShown, setContextShown] = useState(false);
    const contextItems: ([ string, () => void ] | "sep")[] = [
        ["Просмотр", async () => {
            await showModal(EditSubjectModal, { subj: x.item });
        }]
    ];
    return <ContextMenuHolder shown={ contextShown }
                              hide={ () => setContextShown(false) }
                              items={ contextItems }>
        <Button text="Действие"
                onClick={ () => setContextShown(true) }
                presets={ ["regular", "inline"] } />
    </ContextMenuHolder>;
};

export const ViewCurriculumModal: ModalFC<{full: CurriculumFull}, CurriculumFull> = x => {
    const onModalClose = async (): Promise<void> => {
        const confirm = await showModal(ConfirmationModal, {});
        if (confirm)
            x.done(undefined);
    };
    const [filters, setFilters] = useState<boolean>(true);
    const [query, setQuery] = useState("");
    const [course, setCourse] = useState<number>();
    const filterDataset = (dataset: Subject[], query: string, course?: number): Subject[] => {
        const searchResult = query.length
            ? dataset.filter(a => a.subject.toLowerCase().includes(query.toLowerCase()))
            : dataset;
        const filterResult = course
            ? searchResult.filter(a => a.course === course)
            : searchResult;

        return filterResult;
    };
    console.log("в модалке", x.full);

    const validate: boolean = true;
    const columns: Column<Subject>[] = [
        Table.Column("Предмет", a => <>{ a.item.subject }</>),
        Table.Column("Курс", a => <>{ a.item.course }</>, { width: "100px" }),
        Table.Column("Семестр", a => <>{ a.item.semester }</>, { width: "100px" }),
        Table.AutoColumn("Действие", DropdownCell)
    ];
    const courses: SelectOption<number>[] = [1,2,3,4].map(a => ({
        key: a,
        desc: `${a} курс`
    }));
    const title = <HSpread alignItems="center">
        <span>Просмотр учебного плана</span>
        <Button presets={ ["regular"] }
                onClick={ () => setFilters(a => !a) }
                text="Фильтры и поиск" />
    </HSpread>;

    return <CardModal close={ onModalClose }
                      title={ title }
                      padding="20px"
                      bottom={ <HStack>
                          <Expandee />
                          <Button text="Сохранить"
                                  disabled={ !validate }
                                  onClick={ () => x.done(undefined) }
                                  style={ { width: "100px" } }
                                  presets={ ["regular"] } />
                          <Button text="Отмена"
                                  onClick={ () => x.done(undefined) }
                                  style={ { width: "100px" } }
                                  presets={ ["regular", "red"] } />
                      </HStack> }
                      cardHeaderStyle={ {
                          fontSize: "40px",
                          color: "#663A2A",
                          backgroundColor: "white",
                          borderRadius: 0
                      } }
                      width="85%"
                      style={ { height: "85%" } }
                      cardStyle={ { height: "85%" } }>
        <div style={ { padding: "0 10px" } }>
            <VStack spacing="15px">
                {
                    filters
                        && <>
                            <TextBox placeholder="Поиск по предмету" value={ query } onChange={ setQuery } />
                            <ComboBox options={ [{ key: undefined, desc: "Курс" }, ...courses] }
                                      value={ course }
                                      onChangeNonStrict={ setCourse } />
                            <Button text="Сбросить фильтры"
                                    presets={ ["regular"] }
                                    onClick={ () => {setQuery(""); setCourse(undefined);} } />
                        </>
                }
                <Table<Subject> dataset={ filterDataset(x.full.subjects, query, course) } columns={ columns } />
            </VStack>
        </div>
    </CardModal>;
};
