import React from "react";
import { CardModal, ModalFC, showModal } from "components/modals";
import { Expandee, HStack, VStack } from "components/layout/flex";
import { Button } from "components/simple/Button";
import { ConfirmationModal } from "views/modals/ConfirmationModal";


export const EditSubjectModal: ModalFC<{}, boolean> = x => {
    const onModalClose = async (): Promise<void> => {
        const confirm = await showModal(ConfirmationModal, {});
        if (confirm)
            x.done(undefined);
    };

    const validate: boolean = true;

    const title = "Редактировать дисциплину";

    return <CardModal close={ onModalClose }
                      title={ title }
                      padding="20px"
                      bottom={ <HStack>
                          <Expandee />
                          <Button text="Отправить"
                                  disabled={ !validate }
                                  onClick={ () => x.done(true) }
                                  style={ { width: "100px" } }
                                  presets={ ["regular"] } />
                          <Button text="Отмена"
                                  onClick={ () => x.done(undefined) }
                                  style={ { width: "100px" } }
                                  presets={ ["regular", "red"] } />
                      </HStack> }
                      cardHeaderStyle={ {
                          fontSize: "40px",
                          color: "#663A2A",
                          backgroundColor: "white",
                          borderRadius: 0
                      } }
                      width="600px">
        <div>
            <VStack spacing="15px">

            </VStack>
        </div>
    </CardModal>;
};