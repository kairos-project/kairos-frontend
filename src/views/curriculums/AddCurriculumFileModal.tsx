import React, { useState } from "react";
import { CardModal, ModalFC, showModal } from "components/modals";
import { Expandee, HStack, VStack } from "components/layout/flex";
import { Button } from "components/simple/Button";
import { ConfirmationModal } from "views/modals/ConfirmationModal";
import { FilePicker } from "components/simple/FilePicker";
import { Label } from "components/simple/Label";


export const AddCurriculumFileModal: ModalFC<{}, File> = x => {
    const onModalClose = async (): Promise<void> => {
        const confirm = await showModal(ConfirmationModal, {});
        if (confirm)
            x.done(undefined);
    };

    const [file, setFile] = useState<File>();


    const validate: boolean = !!file;

    const title = "Загрузка учебного плана";

    return <CardModal close={ onModalClose }
                      title={ title }
                      padding="20px"
                      bottom={ <HStack>
                          <Expandee />
                          <Button text="Отправить"
                                  disabled={ !validate }
                                  onClick={ () => x.done(file) }
                                  style={ { width: "100px" } }
                                  presets={ ["regular"] } />
                          <Button text="Отмена"
                                  onClick={ () => x.done(undefined) }
                                  style={ { width: "100px" } }
                                  presets={ ["regular", "red"] } />
                      </HStack> }
                      cardHeaderStyle={ {
                          fontSize: "40px",
                          color: "#663A2A",
                          backgroundColor: "white",
                          borderRadius: 0
                      } }
                      width="600px">
        <div>
            <VStack spacing="15px">
                <Label text="Выберите файл учебного плана для загрузки" />
                <FilePicker onChange={ setFile }
                            style={ { width: "500px" } }
                            accept=".xls, .xlsx" />
                <Label presets={ ["hint"] } text="Допустимые форматы файла - .xls, .xlsx" />
            </VStack>
        </div>
    </CardModal>;
};