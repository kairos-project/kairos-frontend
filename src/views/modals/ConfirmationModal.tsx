import React from "react";
import { CardModal, ModalFC } from "components/modals";
import { Label } from "components/simple/Label";
import { Expandee, HStack } from "components/layout/flex";
import { Button } from "components/simple/Button";

interface ConfirmationModalProps {
    text?: string
}

export const ConfirmationModal: ModalFC<ConfirmationModalProps, boolean> = x => {
    return <CardModal close={ () => {
        x.done(false);
    } }
                      title="Подтверждение"
                      padding="20px"
                      bottom={ <HStack>
                          <Expandee />
                          <Button text="Да"
                                  onClick={ () => x.done(true) }
                                  style={ { width: "100px" } }
                                  presets={ ["regular"] } />
                          <Button text="Нет"
                                  onClick={ () => x.done(false) }
                                  style={ { width: "100px" } }
                                  presets={ ["regular"] } />
                      </HStack> }
                      cardHeaderStyle={ {
                          fontSize: "40px",
                          color: "#663A2A",
                          backgroundColor: "white",
                          borderRadius: 0
                      } }
                      width="600px">
        <div>
            <Label text={ x.text ?? "Вы уверены?" } presets={ ["regular", "black"] } />
        </div>
    </CardModal>;
};