import React, { useState } from "react";
import styles from "./Playground.module.css";
import { Button } from "components/simple/Button";
import { Label } from "components/simple/Label";
import { Page } from "components/layout/Page";
import { Column, Table } from "components/table/Table";
import { SelectionTable } from "components/table/SelectionTable";
import { DecimalBox, TextBox } from "components/simple/TextBox";
import Decimal from "decimal.js";
import { Expandee, HStack } from "components/layout/flex";
import { ComboBox } from "components/simple/ComboBox";
import { SelectOption } from "utils/model";

interface Data {
    id: string
    name: string
}
const dataset = [
    { id: "foo", name: "bar" },
    { id: "uwu", name: "owo" },
    { id: "keke", name: "dooo" },
    { id: "phew", name: "emulator" }
];

const columns: Column<Data>[] = [
    Table.Column("#", x => <>{ x.rowIndex }</>),
    Table.Column("id", x => <>{ x.item.id }</> ),
    Table.Column("name", x => <>{ x.item.name }</> )
];

const options: SelectOption<string>[] = [
    {
        desc: "placeholder",
        key: undefined
    },
    {
        desc: "option 1",
        key: "opt1",
        disabled: true
    },
    {
        desc: "option 2",
        key: "opt2",
        disabled: false
    },
    {
        desc: "option 3",
        key: "opt3",
        disabled: false
    }
];

export const PlaygroundView: React.FC = () => {
    const [selected, setSelected] = useState<Data>();
    const [text, setText] = useState("");
    const [decimal, setDecimal] = useState<Decimal>();
    const [option, setOption] = useState<string | undefined>(options[0].key);

    return <Page className={ styles.demo } paper>
        <HStack>
            <TextBox value={ text }
                     placeholder="foo"
                     onChange={ setText }
                     style={ { minHeight: "unset", width: "100px" } } />
            <Expandee />
        </HStack>
        <HStack>
            <TextBox value="disabled"
                     disabled
                     style={ { minHeight: "unset", width: "100px" } } />
        </HStack>
        <HStack>
            <ComboBox options={ options } value={ option } onChangeNonStrict={ a => setOption(a) } />
        </HStack>
        <TextBox value={ text } placeholder="foo" onChange={ setText } style={ { minHeight: "unset" } } />
        <TextBox value="disabled" disabled style={ { minHeight: "unset" } } />
        <DecimalBox value={ decimal } onChange={ setDecimal } style={ { minHeight: "unset" } } />
        <Button text="Regular button"
                presets={ ["regular"] }
                style={ { width: "200px" } } />
        <Button text="Red button"
                presets={ ["red"] }
                style={ { width: "200px" } } />
        <Button text="Disabled button"
                disabled
                presets={ ["red"] }
                style={ { width: "200px" } } />

        <Label text="Header h1" presets={ ["h1", "black"] } />
        <Label text="Header h2" presets={ ["h2", "black"] } />
        <Label text="Header h3" presets={ ["h3", "black"] } />
        <Label text="Text regular" presets={ ["regular", "black"] } />
        <Label text="Text hint" presets={ ["hint", "black"] } />
        <Label text="Text hint accent" presets={ ["hint", "accent"] } />
        <Label text="Text hint accent selected" presets={ ["hint", "accentSelected"] } />
        <div className={ styles.darkest }>
            <Label text="darkest" presets={ ["hint", "white"] } />
        </div>
        <div className={ styles.dark }>
            <Label text="dark" presets={ ["hint", "white"] } />
        </div>
        <div className={ styles.medium }>
            <Label text="medium" presets={ ["hint", "black"] } />
        </div>
        <div className={ styles.light }>
            <Label text="light" presets={ ["hint", "black"] } />
        </div>
        <div className={ styles.accent }>
            <Label text="accent blue" presets={ ["regular", "white"] } />
        </div>
        <div className={ styles.accentDark }>
            <Label text="accent selected blue" presets={ ["regular", "white"] } />
        </div>
        <Table dataset={ dataset } columns={ columns } />
        <div />
        <SelectionTable<Data> dataset={ dataset }
                              selected={ selected }
                              onChange={ setSelected }
                              columns={ columns }
                              mode="single" />
    </Page>;
};