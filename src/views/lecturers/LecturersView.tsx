import React, { useState } from "react";
import { Page } from "components/layout/Page";
import styles from "./LecturersView.module.css";
import { Label } from "components/simple/Label";
import { Column, Table } from "components/table/Table";
import { useMappedStore } from "utils/store";
import { LecturersStore } from "store/lecturers";
import { Lecturer } from "api/models/lecturers/Lecturer";
import { Button } from "components/simple/Button";
import { HStack } from "components/layout/flex";
import { Expandee } from "components/layout/flex";
import { ContextMenuHolder } from "components/simple/ContextMenuHolder";
import { showModal } from "components/modals";
import { ConfirmationModal } from "views/modals/ConfirmationModal";
import { addLecturer, deleteLecturer } from "api/lecturers";
import { AddLecturerModal } from "views/lecturers/AddLecturerModal";
import { backendUrl } from "api/http";

const DropdownCell: React.FC<{item: Lecturer}> = x => {
    const [contextShown, setContextShown] = useState(false);
    const contextItems: ([ string, () => void ] | "sep")[] = [
        ["Ред.", async () => {
            const lecturer = await showModal(AddLecturerModal, { lecturer: x.item });
            if (lecturer) {
                await addLecturer(lecturer);
                await LecturersStore.getLecturers();
            }

        }],
        ["Инд. план", () => window.open(`${backendUrl}/lecturers/${x.item.id}/individualPlan`)],
        "sep",
        ["Удалить", async () => {
            const confirm = await showModal(ConfirmationModal,
                { text: "Вы действительно хотите удалить преподавателя?" });
            if (confirm) {
                await deleteLecturer(x.item.id);
                await LecturersStore.getLecturers();
            }

        }]
    ];
    return <ContextMenuHolder shown={ contextShown }
                              hide={ () => setContextShown(false) }
                              items={ contextItems }>
        <Button text="Действие"
                onClick={ () => setContextShown(true) }
                presets={ ["regular", "inline"] } />
    </ContextMenuHolder>;
};

export const LecturersView: React.FC = () => {
    const dataset = useMappedStore(LecturersStore, x => x.lecturers);

    const columns: Column<Lecturer>[] = [
        Table.AutoColumn("Должность", x => <div>{ x.item.lecturerType.type }</div>),
        Table.AutoColumn("Имя", x => <div>{ x.item.name }</div>),
        Table.AutoColumn("Отчество", x => <div>{ x.item.middleName }</div>),
        Table.AutoColumn("Фамилия", x => <div>{ x.item.lastName }</div>),
        Table.AutoColumn("Ставка", x => <div>{ x.item.lecturerRate.toString() }</div>),
        Table.AutoColumn("Нагрузка на ставку", x => <div>{ x.item.lecturerLoadForRate }</div>),
        Table.AutoColumn("Максимальная нагрузка на ставку", x => <div>{ x.item.lecturerMaxLoadForRate }</div>),
        Table.AutoColumn("Начало работы", x => <div>{ x.item.employmentStartDate.toLocaleDateString() }</div>),
        Table.AutoColumn("Окончание работы", x => <div>{ x.item.employmentFinishDate.toLocaleDateString() }</div>),
        Table.AutoColumn("Действие", DropdownCell)
    ];
    return <Page paper className={ styles.lecturersView }>
        <HStack alignItems="center">
            <Label text="Преподаватели" presets={ ["h3", "black"] } />
            <Expandee />
            <Button text="Добавить"
                    presets={ ["regular"] }
                    onClick={ async () => {
                        const lecturer = await showModal(AddLecturerModal, {});
                        if (lecturer) {
                            await addLecturer(lecturer);
                            await LecturersStore.getLecturers();
                        }

                    } } />
            <Button text="Обновить"
                    onClick={ async () => await LecturersStore.getLecturers() }
                    presets={ ["regular"] } />
        </HStack>
        <Table<Lecturer> dataset={ dataset }
                         columns={ columns } />
    </Page>;
};