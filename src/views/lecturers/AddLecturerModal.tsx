import React, { useState } from "react";
import { CardModal, ModalFC, showModal } from "components/modals";
import { Expandee, Field, HStack, VStack } from "components/layout/flex";
import { Button } from "components/simple/Button";
import { Lecturer, LecturerTemplate } from "api/models/lecturers/Lecturer";
import { ConfirmationModal } from "views/modals/ConfirmationModal";
import { useMappedStore } from "utils/store";
import { LecturersStore } from "store/lecturers";
import { ComboBox } from "components/simple/ComboBox";
import { SelectOption } from "utils/model";
import { LecturerType } from "api/models/lecturers/LecturerType";
import { DecimalBox, TextBox } from "components/simple/TextBox";
import { DatePicker } from "office-ui-fabric-react";
import Decimal from "decimal.js";
import { noop } from "utils/misc";
import { Degree } from "api/models/lecturers/Degree";
import { AcademicRank } from "api/models/lecturers/AcademicRank";

const getNameForLecturerType = (a: LecturerType): string => {
    const main = a.type;
    const add = a.isPartTime
        ? a.isExternal
            ? "внешний совместитель"
            : "внутренний совместитель"
        : "штатный";
    return `${main}, ${add}`;
};

export const AddLecturerModal: ModalFC<{lecturer?: Lecturer}, LecturerTemplate> = x => {
    const onModalClose = async (): Promise<void> => {
        const confirm = await showModal(ConfirmationModal, {
            text: "Все изменения будут утеряны. Вы уверены, что хотите прервать добавление преподавателя?"
        });
        if (confirm)
            x.done(undefined);
    };
    const types: SelectOption<LecturerType>[] = useMappedStore(LecturersStore, x => x.lecturerTypes.map(a => ({
        key: a,
        desc: getNameForLecturerType(a)
    })));
    const degrees: SelectOption<Degree>[] = useMappedStore(LecturersStore, x => x.degrees.map(a => ({
        key: a,
        desc: a.degree
    })));
    const ranks: SelectOption<AcademicRank>[] = useMappedStore(LecturersStore, x => x.academicRanks.map(a => ({
        key: a,
        desc: a.academicRank
    })));
    const isEditing = !!x.lecturer;
    const [type, setType] = useState<LecturerType | undefined>(x.lecturer?.lecturerType);
    const [degree, setDegree] = useState<Degree | undefined>(x.lecturer?.degree);
    const [rank, setRank] = useState<AcademicRank | undefined>(x.lecturer?.academicRank);
    const [name, setName] = useState<string | undefined>(x.lecturer?.name);
    const [lastName, setLastName] = useState<string | undefined>(x.lecturer?.lastName);
    const [middleName, setMiddleName] = useState<string | undefined>(x.lecturer?.middleName);
    const [rate, setRate] = useState<Decimal | undefined>(x.lecturer?.lecturerRate);
    const validateRate = (a: Decimal): boolean => {
        const one = new Decimal(1.01);
        const zero = new Decimal(0);
        if (a.comparedTo(one) === -1)
            if (a.comparedTo(zero) !== -1)
                return true;
        return false;
    };
    const [startDate, setStartDate] = useState<Date | undefined>(x.lecturer?.employmentStartDate);
    const [monthAmount, setMonthAmount] = useState<Decimal | undefined>(x.lecturer?.monthAmount);

    const validate: boolean =  type !== undefined
    && rank !== undefined
    && degree !== undefined
    && name !== undefined
    && lastName !== undefined
    && middleName !== undefined
    && rate !== undefined
    && startDate !== undefined
    && monthAmount !== undefined;

    const createLecturerTemplate = (): LecturerTemplate | undefined => {
        if (validate)
            return {
                id: x.lecturer?.id,
                lecturerTypeId: type!.id,
                name: name!,
                degree: degree!,
                academicRank: rank!,
                middleName: middleName!,
                lastName: lastName!,
                lecturerRate: rate!,
                employmentStartDate: startDate!,
                employmentFinishDate: new Date(),
                monthAmount: monthAmount!
            };
    };
    const title = isEditing ? "Редактировать преподавателя" : "Добавить преподавателя";

    return <CardModal close={ onModalClose }
                      title={ title }
                      padding="20px"
                      bottom={ <HStack>
                          <Expandee />
                          <Button text="Сохранить"
                                  disabled={ !validate }
                                  onClick={ () => x.done(createLecturerTemplate()) }
                                  style={ { width: "100px" } }
                                  presets={ ["regular"] } />
                          <Button text="Отмена"
                                  onClick={ () => x.done(undefined) }
                                  style={ { width: "100px" } }
                                  presets={ ["regular", "red"] } />
                      </HStack> }
                      cardHeaderStyle={ {
                          fontSize: "40px",
                          color: "#663A2A",
                          backgroundColor: "white",
                          borderRadius: 0
                      } }
                      width="1000px">
        <div>
            <VStack spacing="15px">
                <Field alignItems="center" columns={ ["1fr 5fr"] } spacing="15px" title="Имя">
                    <TextBox placeholder="Введите имя"
                             value={ name }
                             disabled={ isEditing }
                             onChange={ setName } />
                </Field>
                <Field alignItems="center" columns={ ["1fr 5fr"] } spacing="15px" title="Фамилия">
                    <TextBox placeholder="Введите фамилию"
                             value={ lastName }
                             disabled={ isEditing }
                             onChange={ setLastName } />
                </Field>
                <Field alignItems="center" columns={ ["1fr 5fr"] } spacing="15px" title="Отчество">
                    <TextBox placeholder="Введите отчество"
                             value={ middleName }
                             disabled={ isEditing }
                             onChange={ setMiddleName } />
                </Field>
                <Field alignItems="center" columns={ ["1fr 5fr"] } spacing="15px" title="Тип преподавателя">
                    <ComboBox<LecturerType>
                        options={ [{ desc: x.lecturer
                            ? getNameForLecturerType(x.lecturer.lecturerType)
                            : "Выберите тип преподавателя", key: undefined },...types] }
                        onChangeNonStrict={ setType }
                        value={ type } />
                </Field>
                <Field alignItems="center" columns={ ["1fr 5fr"] } spacing="15px" title="Ученая степень">
                    <ComboBox<Degree>
                        options={ [{ desc: "Выберите ученую степень", key: undefined },...degrees] }
                        onChangeNonStrict={ setDegree }
                        value={ degree } />
                </Field>
                <Field alignItems="center" columns={ ["1fr 5fr"] } spacing="15px" title="Ученое звание">
                    <ComboBox<AcademicRank>
                        options={ [{ desc: "Выберите ученое звание", key: undefined },...ranks] }
                        onChangeNonStrict={ setRank }
                        value={ rank } />
                </Field>
                <Field alignItems="center" columns={ ["1fr 5fr"] } spacing="15px" title="Ставка">
                    <DecimalBox value={ rate }
                                onChange={ a => a !== undefined
                                    ? validateRate(a)
                                        ? setRate(a)
                                        : noop()
                                    : setRate(undefined) }
                                placeholder="Укажите ставку (значение от 0 до 1)" />
                </Field>
                <Field alignItems="center" columns={ ["1fr 5fr"] } spacing="15px" title="Начало трудоустройства">
                    { /*TODO: add russian locale strings*/ }
                    <DatePicker value={ startDate }
                                initialPickerDate={ new Date() }
                                placeholder="Дата трудоустройства"
                                onSelectDate={ a => setStartDate(a ?? undefined) } />
                </Field>

                <Field alignItems="center" columns={ ["1fr 5fr"] } spacing="15px" title="Срок трудоустройства">
                    <DecimalBox placeholder="Срок трудоустройства (месяцы)"
                                value={ monthAmount }
                                disabled={ startDate === undefined }
                                onChange={ setMonthAmount } />
                </Field>
            </VStack>
        </div>
    </CardModal>;
};