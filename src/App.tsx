import React from "react";
import styles from "./App.module.css";
import animations from "assets/Animations.module.css";
import { Navbar } from "components/layout/Navbar";
import { Sidebar } from "components/layout/Sidebar";
import { Route, Switch, useLocation } from "react-router-dom";
import { PlaygroundView } from "views/Playground";
import { CSSTransition, SwitchTransition } from "react-transition-group";
import { StudentsView } from "views/students/StudentsView";
import { LecturersView } from "views/lecturers/LecturersView";
import { CurriculumsView } from "views/curriculums/CurricilumsView";


export const App: React.FC = () => {
    const location = useLocation();
    // const lecturerTypes = useMappedStore(LecturersStore, x => x.lecturerTypes);
 
    return <div className={ styles.App }>
        <Navbar />
        <Sidebar />
        <div className={ styles.content }>
            <SwitchTransition mode="out-in">
                <CSSTransition
                        key={ location.key }
                        classNames={ {
                            appear: animations.appear,
                            appearActive: animations.appearActive,
                            appearDone: animations.appearDone,
                            enter: animations.enter,
                            enterActive: animations.enterActive,
                            enterDone: animations.enterDone,
                            exit: animations.exit,
                            exitActive: animations.exitActive,
                            exitDone: animations.exitDone
                        } }
                        timeout={ 175 }>
                    <Switch location={ location }>
                        <Route path="/playground" component={ PlaygroundView } />
                        <Route path="/students" component={ StudentsView } />
                        <Route path="/lecturers" component={ LecturersView } />
                        <Route path="/curriculums" component={ CurriculumsView } />
                    </Switch>
                </CSSTransition>
            </SwitchTransition>
        </div>
    </div>;

};
