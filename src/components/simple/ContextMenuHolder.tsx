import React, { CSSProperties, FC } from "react";
import styles from "./ContextMenuHolder.module.css";
import { Label } from "components/simple/Label";

interface ContextMenuHolderProps {
    shown: boolean
    hide: () => void
    items: ([ string, () => void ] | "sep")[]
    align?: "left" | "right"
}

export const ContextMenuHolder: FC<ContextMenuHolderProps> = x => {
    const style = x.shown ? { [x.align ?? "right"]: 0 } as CSSProperties : undefined;

    return <div className={ styles.holder }>
        { x.children }
        {
            x.shown
                && <div className={ styles.menu }
                        tabIndex={ -1 }
                        onBlur={ x.hide }
                        ref={ input => input && input.focus() }
                        style={ style }>
                    {
                        x.items.map((item, i) => {
                            return item === "sep"
                                ? <div className={ styles.sep } key={ i } />
                                : <div className={ styles.item } onClick={ () => {
                                    x.hide();
                                    item[1]();
                                } } key={ i }>
                                    <Label  text={ item[0] } presets={ ["regular"] } />
                                </div>;
                        })
                    }
                </div>
        }
    </div>;
};
