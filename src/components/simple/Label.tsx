import React, { ReactNode } from "react";
import styles from "./Label.module.css";
import { c } from "utils/react/classNames";

export type TextColor = "white" | "black" | "accent" | "accentSelected";
export type LabelPreset =
    TextColor |
    "regular" |
    "hint" |
    "h1" |
    "h2" |
    "h3";

const presets = {
    regular: styles.regular,
    hint: styles.hint,
    h1: styles.h1,
    h2: styles.h2,
    h3: styles.h3,
    white: styles.white,
    black: styles.black,
    accent: styles.accent,
    accentSelected: styles.accentSelected
};
interface LabelProps {
    text: ReactNode
    presets?: LabelPreset[]
    className?: string
}

export const Label: React.FC<LabelProps> = x => {
    const mappedPresets = x.presets ? x.presets.map(p => presets[p]) : [];

    return <div className={ c(styles.label, ...mappedPresets, x.className) }>
        { x.text }
    </div>;
};