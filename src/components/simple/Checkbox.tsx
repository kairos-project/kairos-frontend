import React, { ReactNode } from "react";
import styles from "./Checkbox.module.css";

interface CheckBoxProps {
    disabled?: boolean
    value?: boolean
    onChange?: (value: boolean) => void
    children?: ReactNode
}

export const CheckBox: React.FC<CheckBoxProps> = x => {
    return <input
            className={ styles.input }
            type="checkbox"
            checked={ x.value }
            disabled={ x.disabled }
            readOnly />;
};
