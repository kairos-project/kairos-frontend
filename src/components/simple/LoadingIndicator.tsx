import React from "react";
import { usePromiseTracker } from "react-promise-tracker";
import Loader from "react-loader-spinner";
import styles from "./LoadingIndicator.module.css";

export const LoadingIndicator: React.FC = () => {
    const loading = usePromiseTracker();
    return <div className={ styles.indicator } style={ { opacity: loading.promiseInProgress ? 1 : 0 } }>
        <Loader type="ThreeDots"
                height={ 40 }
                width={ 40 }
                color="#FFFFFF" />
    </div>;
};