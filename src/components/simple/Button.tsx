import React, { CSSProperties, ReactNode } from "react";
import styles from "./Button.module.css";
import { c } from "utils/react/classNames";
import { Label, LabelPreset } from "components/simple/Label";

export type ButtonPreset =
    "regular" |
    "transparent" |
    "inline" |
    "red";

const presets = {
    regular: styles.regular,
    transparent: styles.transparent,
    inline: styles.inline,
    red: styles.red,
    active: styles.active
};

interface MenuButtonProps {
    text: ReactNode
    onClick?: () => void
    presets: ButtonPreset[]
    textPresets?: LabelPreset[]
    disabled?: boolean
    style?: CSSProperties
    className?: string
    active?: boolean
}

export const Button: React.FC<MenuButtonProps> = x => {
    const mappedPresets = x.presets ? x.presets.map(p => presets[p]) : [];
    return (
        <div className={ c(
            styles.button,
            ...mappedPresets,
            x.className,
            x.disabled ? styles.disabled : "",
            x.active ? styles.active : "") }
             style={ x.style }
             onClick={ (x.disabled || x.active) ? undefined : x.onClick }>
            <Label text={ x.text } presets={ x.textPresets ?? ["regular", "white"] } />
        </div>
    );
};