import React, { ChangeEvent, CSSProperties } from "react";
import styles from "./FilePicker.module.css";

interface FilePickerProps {
    accept?: string
    disabled?: boolean
    style?: CSSProperties
    onChange: (a: File) => void
}

export const FilePicker: React.FC<FilePickerProps> = x => {
    const select = (e: ChangeEvent<HTMLInputElement>): void => {
        if (e.target.files?.length)
            x.onChange(e.target.files[0]);
    };

    return  <div className={ styles.upload }>
        <input type="file" onChange={ select } accept={ x.accept } disabled={ x.disabled } style={ x.style } />
    </div>;
};