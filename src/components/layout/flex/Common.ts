export type Orientation = "vertical" | "horizontal";

export type Minus<T, U> = Pick<T, Exclude<keyof T, keyof U>>;
