import React, { CSSProperties, FC } from "react";
import styles from "./Page.module.css";
import { c } from "utils/react/classNames";

interface PageProps {
    style?: CSSProperties
    className?: string
    paper?: boolean
}

export const Page: FC<PageProps> = x => {
    const className = c(
        styles.page,
        x.paper ? styles.paper : "",
        x.className
    );
    return <div style={ x.style } className={ className }>
        { x.children }
    </div>;
};
