import React from "react";
import styles from "./Navbar.module.css";
import { useHistory } from "react-router";
import { LoadingIndicator } from "components/simple/LoadingIndicator";
import { Expandee } from "components/layout/flex";

export const Navbar: React.FC = () => {
    const history = useHistory();
    return <div className={ styles.Navbar }>
        <div className={ styles.logo } onClick={ () => history.push("/") } />
        <Expandee />
        <LoadingIndicator />
    </div>;
};