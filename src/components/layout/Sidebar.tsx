import React, { useState } from "react";
import styles from "./Sidebar.module.css";
import { Button } from "components/simple/Button";
import { Expandee } from "components/layout/flex";
import { c } from "utils/react/classNames";
import { useHistory } from "react-router";
import { LecturersStore } from "store/lecturers";

export const Sidebar: React.FC = () => {
    const history = useHistory();
    const [collapsed, setCollapsed] = useState(false);
    return <div className={ c(styles.Sidebar, collapsed ? styles.collapsed : "") }>
        <div className={ styles.links }>
            <Button text="Учебные планы"
                    presets={ ["transparent"] }
                    style={ { width: "100%" } }
                    onClick={ async () => {
                        await LecturersStore.getLecturers();
                        history.push("/curriculums");
                    } }
                    active={ window.location.pathname.includes("curriculums") } />
            <Button text="Преподаватели"
                    presets={ ["transparent"] }
                    style={ { width: "100%" } }
                    onClick={ async () => {
                        await LecturersStore.getLecturers();
                        history.push("/lecturers");
                    } }
                    active={ window.location.pathname.includes("lecturers") } />
            <Button text="Студенты"
                    presets={ ["transparent"] }
                    style={ { width: "100%" } }
                    onClick={ () => history.push("/students") }
                    active={ window.location.pathname.includes("students") } />
            <Button text="Playground"
                    presets={ ["transparent"] }
                    style={ { width: "100%" } }
                    onClick={ () => history.push("/playground") }
                    active={ window.location.pathname.includes("playground") } />
        </div>
        <Expandee />
        <div className={ styles.controls }>
            <Expandee />
            <Button text={ "<" }
                    onClick={ () => setCollapsed(a => !a) }
                    className={ collapsed
                        ? styles.collapsedButton
                        : styles.uncollapsedButton }
                    presets={ ["transparent"] } />
        </div>
    </div>;
};