import React, { CSSProperties, FC, useContext } from "react";
import styles from "./ModalBase.module.css";
import { ModalDestroyContext } from "components/modals/ModalPresenter";
import { c } from "utils/react/classNames";

interface ModalBaseProps {
    readonly useScrim: boolean
    readonly close: () => void
}

export const ModalBase: FC<ModalBaseProps> = x => {
    const [destroying, destroyTimeout] = useContext(ModalDestroyContext);

    return <div className={ c(styles.modalScrim, destroying ? styles.modalDestroying : styles.modalActive) }
                style={ {
                    background: x.useScrim ? "#00000060" : undefined,
                    "--destroyTimeout": destroyTimeout + "ms"
                } as CSSProperties }
                onClick={ x.close }>
        { x.children }
    </div>;
};