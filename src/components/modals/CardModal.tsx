import React, { CSSProperties } from "react";
import { ModalBase } from "components/modals";
import { Card } from "components/simple/Card";
import styles from "./CardModal.module.css";

interface CardModalProps {
    title: React.ReactNode
    bottom?: React.ReactNode
    close: (() => void) | "disabled"
    children: React.ReactNode
    width: string
    style?: CSSProperties
    cardStyle?: CSSProperties
    cardHeaderStyle?: CSSProperties
    cardContentStyle?: CSSProperties
    padding?: string
}

const nop = (): void => {};

export const CardModal: React.FC<CardModalProps> = x => {
    const close = x.close === "disabled" ? undefined : x.close;

    return <ModalBase useScrim={ true } close={ close ?? nop }>
        <div className={ styles.wrapper } style={ { width: x.width, ...x.style } }>
            <Card onClick={ e => e.stopPropagation() }
                  style={ { overflow: "hidden", ...x.cardStyle } }
                  headerStyle={ x.cardHeaderStyle }
                  contentStyle={ { overflowY: "auto", ...x.cardContentStyle } }
                  padding={ x.padding }
                  bottom={ x.bottom } title={ x.title } hasCloseButton={ !!close } onClose={ close }>
                { x.children }
            </Card>
        </div>
    </ModalBase>;
};